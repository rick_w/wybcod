package com.eipihany.wybcod.state_pattern;

/**
 * @ClassName: HasQuarterState
 * @Description: 投硬币的状态
 * @Author: wwangyb
 * @Date: 2020-08-06 21:39
 * @Version: 1.0.0
 */
public class HasQuarterState extends State {
    GumballMachine gumballMachine;
    public HasQuarterState(GumballMachine gumballMachine){
        this.gumballMachine = gumballMachine;
    }
    @Override
    public void insertQuarter() {
        System.out.println("请不要重复投币！");
        //退币
        returnQuarter();
    }

    @Override
    public void ejectQuarter() {
        //退币
        returnQuarter();
        //退币转换成没有硬币的状态
        gumballMachine.setState(gumballMachine.noQuarterState);
    }

    @Override
    public void turnCrank() {
        System.out.println("转动曲轴，准备发糖");
        //转换成发糖的状态
        gumballMachine.setState(gumballMachine.soldState);
    }

    @Override
    public void dispense() {
        System.out.println("this method dont`t support");
    }
}
