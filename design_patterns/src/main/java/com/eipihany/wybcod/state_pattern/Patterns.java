package com.eipihany.wybcod.state_pattern;

/**
 * @ClassName: Patterns
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2020-08-06 22:02
 * @Version: 1.0.0
 */
public class Patterns {
    public static void main(String[] args) {
        GumballMachine gumballMachine = new GumballMachine(10);
        gumballMachine.insertQuarter();//投入硬币
        gumballMachine.turnCrank();//转动曲轴
        gumballMachine.dispense();//出售糖果
    }
}
