package com.eipihany.freemarker.bean;

import lombok.ToString;

/**
 * @ClassName: User
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2020-08-30 19:52
 * @Version: 1.0.0
 */
@ToString
public class User {
    private Long id;
    private String username;
    private String address;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
