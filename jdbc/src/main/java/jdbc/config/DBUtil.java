package jdbc.config;

import java.sql.*;

/**
 * @ClassName: DBUtil
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2019-10-23 21:36
 * @Version: 1.0.0
 */
public class DBUtil {
    //mysql驱动包名
    private static final String DRIVE_NAME = "com.mysql.jdbc.Driver";
    //数据库连接地址
    private static final String URL = "jdbc:mysql://47.103.68.73:3306/vhr";
    //用户名
    private static final String USERNAME = "root";
    //密码
    private static final String PASSWORD = "root";

    public static Connection connection = null;
    public static Statement statement = null;
    public static ResultSet resultSet = null;

    /**
     * 创建数据库连接
     * @return
     * @throws Exception
     */
    public static Connection getConnection() throws Exception {
        //加载mysql驱动类
        Class.forName(DRIVE_NAME);
        //获取数据库连接
        connection = DriverManager.getConnection(URL,USERNAME,PASSWORD);

        return connection;
    }

    /**
     * 执行update,delete,insert操作
     * @param sql
     * @return
     * @throws Exception
     */
    public static int executeUpdate(String sql) throws Exception {
        int result = 0;
        statement = getConnection().createStatement();
        result = statement.executeUpdate(sql);
        return result;
    }

    /**
     * 执行select语句
     * @param sql
     * @return
     */
    public static ResultSet executeQuery(String sql){
        try {
            statement = getConnection().createStatement();
            resultSet = statement.executeQuery(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultSet;
    }

    public static void close() throws SQLException {
        if (resultSet != null){
            resultSet.close();
        }
        if (statement != null){
            statement.close();
        }
        if(connection != null){
            connection.close();
        }
    }


    public static void main(String[] args) {
        try {
            getConnection();
            //mysql查询语句
            String sql = "select * from hr";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            //结果集
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                System.out.println("名称："+resultSet.getString("name"));
            }
            resultSet.close();
            preparedStatement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (connection != null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
