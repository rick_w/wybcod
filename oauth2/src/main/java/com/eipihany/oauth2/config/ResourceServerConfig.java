package com.eipihany.oauth2.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

/**
 * @ClassName: ResourceServerConfig
 * @Description: 资源服务器
 * @Author: wwangyb
 * @Date: 2020-08-08 17:07
 * @Version: 1.0.0
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        //配置资源id，这里的id和授权服务器的资源id要一样
        resources.resourceId("rid")
                .stateless(true);//stateless设置这些资源仅基于令牌验证
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers("/wybcod/**").hasRole("wybcod")
                .antMatchers("eipihany/**").hasRole("eipihany")
                .anyRequest().authenticated();
    }
}
