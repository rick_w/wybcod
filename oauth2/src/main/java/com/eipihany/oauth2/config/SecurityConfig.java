package com.eipihany.oauth2.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @ClassName: SecurityConfig
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2020-08-08 17:09
 * @Version: 1.0.0
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Bean
    @Override
    protected UserDetailsService userDetailsService() {
        return super.userDetailsService();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("eipihany")
                .password("$2a$10$Bi8ksaO4JK6XJ03WCeA/0erL9/9FKU6Utdck4e7Xxon8WiZO9zNIO")
                .roles("eipihany")
                .and()
                .withUser("wybcod")
                .password("$2a$10$Bi8ksaO4JK6XJ03WCeA/0erL9/9FKU6Utdck4e7Xxon8WiZO9zNIO")
                .roles("wybcod");
    }

    /**
     *
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.antMatcher("/oauth/**")
                .authorizeRequests()
                .antMatchers("/oauth/**").permitAll()
                .and().csrf().disable();
    }
}
