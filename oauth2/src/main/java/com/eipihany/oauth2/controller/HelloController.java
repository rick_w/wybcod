package com.eipihany.oauth2.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: HelloController
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2020-08-08 17:14
 * @Version: 1.0.0
 */
@RestController
public class HelloController {
    @GetMapping("/eipihany/hello")
    public String eipihany(){
        return "Hello eipihany";
    }

    @GetMapping("/wybcod/hello")
    public String wybcod(){
        return "Hello wybcod";
    }

    @GetMapping("/hello")
    public String hello(){
        return "Hello";
    }
}
