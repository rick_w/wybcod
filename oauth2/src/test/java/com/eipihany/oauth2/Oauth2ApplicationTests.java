package com.eipihany.oauth2;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootTest
class Oauth2ApplicationTests {

    @Test
    void contextLoads() {
        System.out.println(new BCryptPasswordEncoder().encode("123"));
        System.out.println(new BCryptPasswordEncoder().upgradeEncoding("$2a$10$Bi8ksaO4JK6XJ03WCeA/0erL9/9FKU6Utdck4e7Xxon8WiZO9zNIO"));
    }
}
