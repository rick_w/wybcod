import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * @ClassName: RedisPong
 * @Description: TODO
 * @Author: epiphany
 * @Date: 2020-07-27 00:22
 * @Version: 1.0.0
 */
public class RedisPong {
    public static void main(String[] args) throws InterruptedException {
        /*Jedis jedis = new Jedis("192.168.1.101",6379);
        String ping = jedis.ping();
        System.out.println(ping);*/
        //对象池
        GenericObjectPoolConfig<String> config = new GenericObjectPoolConfig<String>();
        config.setMaxTotal(100);
        config.setMaxIdle(20);
        JedisPool jedisPool = new JedisPool(config,"192.168.1.101",6379);
        Jedis jedis = jedisPool.getResource();
        System.out.println(jedis.ping());
        jedis.set("k1","1");
        jedis.set("k2","2");
        jedis.set("k3","3");
        jedis.set("k4","4");
        jedis.set("k5","5");
        jedis.set("k6","6");
        System.out.println(jedis.keys("k*"));
        System.out.println(jedis.get("k1"));
        System.out.println(jedis.type("k1"));
        System.out.println(jedis.expire("k1",20));
        Thread.sleep(500);
        System.out.println(jedis.ttl("k1"));
    }
}
