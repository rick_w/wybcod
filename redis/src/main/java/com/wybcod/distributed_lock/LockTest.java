package com.wybcod.distributed_lock;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.params.SetParams;

/**
 * @ClassName: LockTest
 * @Description: 分布式锁
 * @Author: epiphany
 * @Date: 2020-07-30 23:02
 * @Version: 1.0.0
 */
public class LockTest {
    public static void main(String[] args) {
        Jedis jedis = new Jedis("192.168.1.101",6379);
        Long sent = jedis.setnx("c1","v1");
        jedis.set("k1","v1",new SetParams().nx().px(5));
        if (sent == 1){
            //给锁添加过期时间，防止应用在运行过程中抛出异常导致锁无法释放
            jedis.expire("c1",5);
            //没人占位
            jedis.set("name","epiphany");
            System.out.println(jedis.get("name"));
            //释放资源
            jedis.del("c1");
        }else{
            System.out.println("有人占位，停止操作");
            //有人占位，停止操作
        }
    }
}
