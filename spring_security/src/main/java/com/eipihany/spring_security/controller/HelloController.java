package com.eipihany.spring_security.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: HelloController
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2020-08-17 21:23
 * @Version: 1.0.0
 */
@RestController
public class HelloController {
    @GetMapping("/hello")
    public String hello(){
        return "Hello Word!";
    }
    @RequestMapping("/s")
    public String success(){
        return "success";
    }
    @RequestMapping("/f")
    public String fail(){
        return "fail";
    }
}
