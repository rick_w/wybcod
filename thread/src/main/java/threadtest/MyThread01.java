package threadtest;

/**
 * @ClassName: MyThread01
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2019-07-19 22:21
 * @Version: 1.0.0
 */
public class MyThread01 extends Thread {
    @Override
    public void run(){
        try {
            for (int i = 0; i < 10; i++) {
                int time = (int) (Math.random()*1000);
                Thread.sleep(time);
                System.out.println("当前线程名称="+Thread.currentThread().getName());
            }
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
