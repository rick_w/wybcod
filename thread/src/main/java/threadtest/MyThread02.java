package threadtest;

/**
 * @ClassName: MyThread02
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2019-07-19 22:26
 * @Version: 1.0.0
 */
public class MyThread02 extends Thread {
    public int i;
    public MyThread02(int i){
        super();
        this.i = i;
    }

    @Override
    public void run(){
        System.out.println("当前数字："+i);
    }
}
