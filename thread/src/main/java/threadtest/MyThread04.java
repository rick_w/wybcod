package threadtest;

/**
 * @ClassName: MyThread04
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2019-07-21 18:31
 * @Version: 1.0.0
 */
public class MyThread04 extends Thread {

    private int count = 5;

    @Override
    synchronized public void run(){
        super.run();
        count--;
        System.out.println("由"+Thread.currentThread().getName()+"计算，count="+count);
    }
}
