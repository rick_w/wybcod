package threadtest;

/**
 * @ClassName: MyThread06
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2019-07-21 19:07
 * @Version: 1.0.0
 */
public class MyThread06 extends Thread {
    public MyThread06(){
        System.out.println("构造方法的打印："+Thread.currentThread().getName());
    }
    @Override
    public void run(){
        System.out.println("run方法的打印："+Thread.currentThread().getName());
    }
}
