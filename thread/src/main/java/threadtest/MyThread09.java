package threadtest;

/**
 * @ClassName: MyThread09
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2019-07-21 19:20
 * @Version: 1.0.0
 */
public class MyThread09 extends Thread {

    public MyThread09(){
        System.out.println("构造方法-----开始");
        System.out.println("Thread.currentThread().getName()="+Thread.currentThread().getName());
        System.out.println("Thread.currentThread().isAlive()="+Thread.currentThread().isAlive());
        System.out.println("this.getName()="+this.getName());
        System.out.println("this.isAlive()="+this.isAlive());
        System.out.println("构造方法-----结束");
    }
    @Override
    public void run(){
        System.out.println("run方法----开始");
        System.out.println("Thread.currentThread().getName()="+Thread.currentThread().getName());
        System.out.println("Thread.currentThread().isAlive()="+Thread.currentThread().isAlive());
        System.out.println("this.getName()="+this.getName());
        System.out.println("this.isAlive()="+this.isAlive());
        System.out.println("run方法----结束");
    }
}
