package threadtest;

/**
 * @ClassName: MyThread10
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2019-07-21 19:25
 * @Version: 1.0.0
 */
public class MyThread10 extends Thread {
    @Override
    public void run(){
        try {
            System.out.println("正在运行的线程名称："+Thread.currentThread().getName()+" 开始");
            Thread.sleep(2000);
            System.out.println("正在运行的线程名称："+Thread.currentThread().getName()+" 结束");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
