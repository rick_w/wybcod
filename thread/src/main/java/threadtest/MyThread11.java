package threadtest;

/**
 * @ClassName: MyThread11
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2019-07-21 19:33
 * @Version: 1.0.0
 */
public class MyThread11 extends Thread {

    @Override
    public void run(){
        try {
            System.out.println("正在运行的线程名称："+Thread.currentThread().getName()+" 开始时间="+System.currentTimeMillis());
            Thread.sleep(2000);
            System.out.println("正在运行的线程名称："+Thread.currentThread().getName()+" 结束时间="+System.currentTimeMillis());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
