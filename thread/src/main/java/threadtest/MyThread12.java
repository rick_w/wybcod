package threadtest;

/**
 * @ClassName: MyThread12
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2019-07-21 19:37
 * @Version: 1.0.0
 */
public class MyThread12 extends Thread {
    @Override
    public void run(){
        long beginTime = System.currentTimeMillis();
        System.out.println("线程开始执行时间："+beginTime);
        int count=0;
        for (int i = 0; i < 50000000 ; i++) {
            Thread.yield();
            count = count + (i+1);
        }
        long endTime = System.currentTimeMillis();
        System.out.println("线程结束执行时间："+endTime);
        System.out.println("本次执行用时："+(endTime-beginTime)+"毫秒！");
    }
}
