package threadtest;

/**
 * @ClassName: MyThread15
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2019-07-21 19:58
 * @Version: 1.0.0
 */
public class MyThread15 extends Thread {
    @Override
    public void run(){
        super.run();
        try {
            for (int i = 0; i < 500000 ; i++) {
                if(this.interrupted()){
                    System.out.println("当前线程已停止！");
                    throw new InterruptedException();
                }
                System.out.println("i="+(i+1));
            }
            System.out.println("我在for下面");
        } catch (InterruptedException e) {
            System.out.println("进入MyThread15.java类run方法的catch了！");
            e.printStackTrace();
        }
    }
}
