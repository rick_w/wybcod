package threadtest;

/**
 * @ClassName: MyThread16
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2019-07-21 20:07
 * @Version: 1.0.0
 */
public class MyThread16 extends Thread {
    @Override
    public void run(){
        super.run();
        try {
            System.out.println("run begin");
            Thread.sleep(200000);
            System.out.println("run end");
        } catch (InterruptedException e) {
            System.out.println("在休眠中被停止！进入catch！"+this.isInterrupted());
            e.printStackTrace();
        }
    }
}
