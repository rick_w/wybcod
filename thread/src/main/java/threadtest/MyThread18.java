package threadtest;

/**
 * @ClassName: MyThread18
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2019-07-21 20:16
 * @Version: 1.0.0
 */
public class MyThread18 extends Thread {

    private int i = 0;

    @Override
    public void run(){
        try {
            while (true){
                i++;
                System.out.println("i="+i);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
