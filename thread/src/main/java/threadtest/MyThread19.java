package threadtest;

/**
 * @ClassName: MyThread19
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2019-07-21 20:21
 * @Version: 1.0.0
 */
public class MyThread19 extends Thread {

    private SynchronizedObject object;

    public MyThread19(SynchronizedObject object){
        super();
        this.object = object;
    }

    @Override
    public void run(){
        object.printString("admin","123456");
    }
}
