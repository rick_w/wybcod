package threadtest;

/**
 * @ClassName: MyThread20
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2019-07-21 20:27
 * @Version: 1.0.0
 */
public class MyThread20 extends Thread {
    @Override
    public void run(){
        while (true){
            if(this.isInterrupted()){
                System.out.println("停止了");
                return;
            }
            System.out.println("timer="+System.currentTimeMillis());
        }
    }
}
