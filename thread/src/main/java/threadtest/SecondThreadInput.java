package threadtest;

/**
 * @ClassName: SecondThreadInput
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2019-07-22 21:19
 * @Version: 1.0.0
 */
public class SecondThreadInput implements Runnable {
    @Override
    public void run() {
        System.out.println("调用SecondThreadInput类的run（）重写方法");
        for (int i = 0; i < 5 ; i++) {
            System.out.println("SecondThreadInput线程中i="+i);
            try {
                Thread.sleep((long) (Math.random()*100));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
