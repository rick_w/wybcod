package threadtest;

/**
 * @ClassName: SynchronizedObject1
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2019-07-22 20:53
 * @Version: 1.0.0
 */
public class SynchronizedObject1 {
    synchronized public void printString(){
        System.out.println("begin");
        if (Thread.currentThread().getName().equals("a")){
            System.out.println("a线程永远 suspend了！");
            Thread.currentThread().suspend();
        }
        System.out.println("end");
    }
}
