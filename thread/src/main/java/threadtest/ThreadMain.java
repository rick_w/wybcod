package threadtest;

/**
 * @ClassName: ThreadMain
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2019-07-19 22:17
 * @Version: 1.0.0
 */
public class ThreadMain {
    public static void main(String[] args) throws InterruptedException {
        /*try {
            MyThread01 myThread = new MyThread01();
            myThread.setName("myThread");
            myThread.start();
            for (int i = 0; i < 10; i++) {
                int time = (int) (Math.random()*1000);
                Thread.sleep(time);
                System.out.println("当前线程名称="+Thread.currentThread().getName());
            }
        }catch (InterruptedException e){
            e.printStackTrace();
        }*/

        /*MyThread02 myThread021 = new MyThread02(1);
        MyThread02 myThread022 = new MyThread02(2);
        MyThread02 myThread023 = new MyThread02(3);
        MyThread02 myThread024 = new MyThread02(4);
        MyThread02 myThread025 = new MyThread02(5);
        MyThread02 myThread026 = new MyThread02(6);
        MyThread02 myThread027 = new MyThread02(7);
        MyThread02 myThread028 = new MyThread02(8);
        MyThread02 myThread029 = new MyThread02(9);
        MyThread02 myThread0210 = new MyThread02(10);
        myThread021.start();
        myThread022.start();
        myThread023.start();
        myThread024.start();
        myThread025.start();
        myThread026.start();
        myThread027.start();
        myThread028.start();
        myThread029.start();
        myThread0210.start();*/

        /*Runnable runnable = new MyRunnable();
        Thread thread = new Thread(runnable);
        thread.start();
        System.out.println("主线程运行结束");*/

        /*MyThread03 A = new MyThread03("A");
        MyThread03 B = new MyThread03("B");
        MyThread03 C = new MyThread03("C");
        A.start();
        B.start();
        C.start();*/

        /*MyThread04 myThread04 = new MyThread04();
        Thread a = new Thread(myThread04,"A");
        Thread b = new Thread(myThread04,"B");
        Thread c = new Thread(myThread04,"C");
        Thread d = new Thread(myThread04,"D");
        a.start();
        b.start();
        c.start();
        d.start();*/

        /*LogicThreadA logicThreadA = new LogicThreadA();
        logicThreadA.start();
        LogicThreadB logicThreadB = new LogicThreadB();
        logicThreadB.start();*/

        /*MyThread05 run = new MyThread05();
        Thread t1 = new Thread(run);
        Thread t2 = new Thread(run);
        Thread t3 = new Thread(run);
        Thread t4 = new Thread(run);
        Thread t5 = new Thread(run);
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();*/

        /*System.out.println(Thread.currentThread().getName());*/

        /*MyThread06 myThread06 = new MyThread06();
        myThread06.start();
        myThread06.run();*/

        /*MyThread07 myThread07 = new MyThread07();
        Thread t1 = new Thread(myThread07);
        t1.setName("A");
        t1.start();*/

        /*MyThread08 myThread08 = new MyThread08();
        System.out.println("begin="+myThread08.isAlive());
        myThread08.start();
        Thread.sleep(1000);
        System.out.println("end="+myThread08.isAlive());*/

        /*MyThread09 myThread09 = new MyThread09();
        Thread thread = new Thread(myThread09);
        System.out.println("main begin thread isAlive="+thread.isAlive());
        thread.setName("A");
        thread.start();
        System.out.println("main ben thread isAlive="+thread.isAlive());*/

        /*MyThread10 myThread10 = new MyThread10();
        System.out.println("主线程开始时间="+System.currentTimeMillis());
        myThread10.start();
        System.out.println("主线程结束时间="+System.currentTimeMillis());*/

        /*MyThread11 myThread11 = new MyThread11();
        System.out.println("主线程开始时间="+System.currentTimeMillis());
        myThread11.start();
        System.out.println("主线程结束时间="+System.currentTimeMillis());*/

        /*Thread run = Thread.currentThread();
        System.out.println("当前线程名称："+run.getName());
        System.out.println("当前线程标识："+run.getId());*/

        /*MyThread12 myThread12 = new MyThread12();
        myThread12.start();*/

        /*try {
            MyThread13 myThread13 = new MyThread13();
            myThread13.start();
            Thread.sleep(100);
            myThread13.interrupt();
        } catch (InterruptedException e) {
            System.out.println("main catch");
            e.printStackTrace();
        }*/

        /*try {
            MyThread14 myThread14 = new MyThread14();
            myThread14.start();
            Thread.sleep(100);
            myThread14.interrupt();
            System.out.println("是否停止1？="+myThread14.interrupted());
            System.out.println("是否停止2？="+myThread14.interrupted());
        } catch (InterruptedException e) {
            System.out.println("main catch");
            e.printStackTrace();
        }
        System.out.println("end!");*/

        /*Thread.currentThread().interrupt();
        System.out.println("是否停止1？="+Thread.interrupted());
        System.out.println("是否停止2？="+Thread.interrupted());
        System.out.println("end!");*/

        /*try {
            MyThread14 myThread14 = new MyThread14();
            myThread14.start();
            Thread.sleep(100);
            myThread14.interrupt();
            System.out.println("是否停止1？="+myThread14.isInterrupted());
            System.out.println("是否停止2？="+myThread14.isInterrupted());
        } catch (InterruptedException e) {
            System.out.println("main catch");
            e.printStackTrace();
        }
        System.out.println("end!");*/

        /*try {
            MyThread15 myThread15 = new MyThread15();
            myThread15.start();
            Thread.sleep(2000);
            myThread15.interrupt(); //停止线程
        } catch (InterruptedException e) {
            //捕获线程停止异常
            System.out.println("main catch");
            e.printStackTrace();
        }
        System.out.println("end!");*/

        /*try {
            MyThread16 myThread16 = new MyThread16();
            myThread16.start();
            Thread.sleep(2000);
            myThread16.interrupt();
        } catch (InterruptedException e) {
            System.out.println("main catch");
            e.printStackTrace();
        }
        System.out.println("end!");*/

        /*MyThread17 myThread17 = new MyThread17();
        myThread17.start();
        myThread17.interrupt();
        System.out.println("end!");*/

        /*try {
            MyThread18 myThread18 = new MyThread18();
            myThread18.start();
            Thread.sleep(8000);
            myThread18.stop();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        /*try {
            SynchronizedObject synchronizedObject = new SynchronizedObject();
            MyThread19 myThread19 = new MyThread19(synchronizedObject);
            myThread19.start();
            Thread.sleep(500);
            myThread19.stop();
            System.out.println("用户名："+synchronizedObject.getUsername());
            System.out.println("密码："+synchronizedObject.getPassword());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        /*try {
            MyThread20 myThread20 = new MyThread20();
            myThread20.start();
            Thread.sleep(2000);
            myThread20.interrupt();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        /*try {
            MyThread21 myThread21 = new MyThread21();
            myThread21.start();
            Thread.sleep(5000);
            //A段
            myThread21.suspend();
            System.out.println("A="+System.currentTimeMillis()+" i= "+myThread21.getI());
            Thread.sleep(5000);
            System.out.println("A="+System.currentTimeMillis()+" i= "+myThread21.getI());
            //B段
            myThread21.resume();
            Thread.sleep(5000);
            //C段
            myThread21.suspend();
            System.out.println("B="+System.currentTimeMillis()+" i= "+myThread21.getI());
            Thread.sleep(5000);
            System.out.println("B="+System.currentTimeMillis()+" i= "+myThread21.getI());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        /*try {
            final SynchronizedObject1 object = new SynchronizedObject1();
            Thread thread = new Thread(){
                @Override
                public void run(){
                  object.printString();
                }
            };
            thread.setName("a");
            thread.start();
            Thread.sleep(1000);
            Thread thread1 = new Thread(){
              @Override
              public void run(){
                  System.out.println("thread1启动了，但进入不了printString()方法！所以只会打印一个begin！");
                  System.out.println("因为pringString()方法被a线程锁定并且永久暂停了！");
                  object.printString();
              }
            };
            thread1.start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        /*try {
            MyThread22 myThread22 = new MyThread22();
            myThread22.start();
            Thread.sleep(1000);
            myThread22.suspend();
            System.out.println("main end!");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        /*final MyObject myObject = new MyObject();
        Thread thread = new Thread(){
            public void run(){
                myObject.setValue("a","aa");
            }
        };
        thread.setName("a");
        thread.start();
        Thread.sleep(500);
        Thread thread1 = new Thread(){
            public void run(){
                myObject.printUsernamePassword();
            }
        };
        thread1.start();*/

        /*FirstThreadInput firstThreadInput = new FirstThreadInput();
        Thread thread = new Thread(new SecondThreadInput());
        firstThreadInput.setPriority(1);
        thread.setPriority(10);
        firstThreadInput.start();
        thread.start();*/
    }
}
