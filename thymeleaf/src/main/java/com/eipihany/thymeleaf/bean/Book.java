package com.eipihany.thymeleaf.bean;

/**
 * @ClassName: Book
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2020-08-30 20:34
 * @Version: 1.0.0
 */
public class Book {
    private Long id;
    private String name;
    private String author;
    private Double price;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
