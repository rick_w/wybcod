package com.eipihany.thymeleaf.controller;

import com.eipihany.thymeleaf.bean.Book;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: BookController
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2020-08-30 20:35
 * @Version: 1.0.0
 */
@Controller
public class BookController {
    @GetMapping("/")
    public String book(Model model){
        List<Book> books = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Book book = new Book();
            book.setId((long)i);
            book.setName("三国演义："+i);
            book.setAuthor("罗贯中："+i);
            book.setPrice((double)30);
            books.add(book);
        }
        model.addAttribute("books",books);
        return "book";
    }
}
